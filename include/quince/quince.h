#ifndef QUINCE__quince_h
#define QUINCE__quince_h

/*
    Copyright 2014 Michael Shepanski

    This file is part of the quince library.

    Quince is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Quince is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with quince.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <quince/detail/grouping.h>
#include <quince/detail/junction.h>
#include <quince/exprn_mappers/expressions.h>
#include <quince/mappers/mappers.h>
#include <quince/database.h>
#include <quince/define_mapper.h>
#include <quince/exceptions.h>
#include <quince/mapping_customization.h>
#include <quince/query.h>
#include <quince/serial.h>
#include <quince/table.h>
#include <quince/table_alias.h>
#include <quince/transaction.h>

#endif

